# [SRRG2 PROSLAM](srrg2_proslam) | [Wiki](https://gitlab.com/srrg-software/srrg2_proslam/wikis/home)
[![pipeline status](https://gitlab.com/srrg-software/srrg2_proslam/badges/master/pipeline.svg)](https://gitlab.com/srrg-software/srrg2_proslam/commits/master)

This package packages packages.

Examples: https://gitlab.com/srrg-software/srrg2_executor

# [SRRG2 PROSLAM ROS](srrg2_proslam_ros) | [Wiki](https://gitlab.com/srrg-software/srrg2_proslam_ros/wikis/home)

This package packages packages.

## Contributors

Io so io.

## License

BSD 4.0
