"SparseBlockLinearSolverCholeskyCSparse" { 
  "#id" : 1
 }

"MergerCorrespondencePointIntensityDescriptor3f" { 
  "#id" : 2, 

  // toggles point binning (distribution homogenization)
  "enable_binning" : 1, 

  // maximum distance in geometry (i.e. 3D point distance norm) in meters (squared)
  "maximum_distance_geometry_squared" : 0.25, 

  // maximum permitted correspondence response for merging a point
  "maximum_response" : 50, 

  // target number of points to merge, if hit no further points without correspondences will be added to moving (i.e. determines the pool of trackable points)
  "target_number_of_merges" : 200
 }

"CorrespondenceFinderDescriptorBasedBruteforce3D3D" { 
  "#id" : 3, 
  "name" : "cf_bruteforce_3d", 

  // maximum permitted descriptor distance for a match
  "maximum_descriptor_distance" : 35, 

  // Lowe's distance to drop ambiguous match candidates
  "maximum_distance_ratio_to_second_best" : 0.699999988, 

  // desired minimum matching ratio with current configuration (signals transgressions)
  "minimum_matching_ratio" : 0.25
 }

"MessageFileSource" { 
  "#id" : 4, 
  "name" : "source", 

  // file to read
  "filename" : "messages.json"
 }

"ProjectiveDepthPointEKF3D" { 
  "#id" : 5, 
  "name" : "point_filter"
 }

"PointIntensityDescriptor3fProjectorPinhole" { 
  "#id" : 6, 
  "name" : "projector", 

  // maximum range [m]
  "range_max" : 7.5, 

  // minimum range [m]
  "range_min" : 0.00999999978
 }

"MultiRelocalizer3D" { 
  "#id" : 7, 

  // aligner unit used to determine the best nearby local map for relocalization
  "aligner" : { 
    "#pointer" : 8
   }, 

  // max translation to attempt a jump
  "max_translation" : 1, 

  // maximum chi per inlier for sucessful relocalization
  "relocalize_max_chi_inliers" : 100, 

  // minimum number of inliers for sucessful relocalization
  "relocalize_min_inliers" : 40, 

  // minimum fraction of inliers out of the total correspondences
  "relocalize_min_inliers_ratio" : 0.5
 }

"Solver" { 
  "#id" : 9, 
  "actions" : [  ], 

  // pointer to the optimization algorithm (GN/LM or others)
  "algorithm" : { 
    "#pointer" : 10
   }, 

  // pointer to linear solver used to compute Hx=b
  "linear_solver" : { 
    "#pointer" : 1
   }, 
  "max_iterations" : [ 1 ], 

  // Minimum mean square error variation to perform global optimization
  "mse_threshold" : -1, 
  "robustifier_policies" : [  ], 

  // term criteria ptr, if 0 solver will do max iterations
  "termination_criteria" : { 
    "#pointer" : 11
   }, 

  // turn it off to make the world a better place
  "verbose" : 0
 }

"SimpleTerminationCriteria" { 
  "#id" : 12, 

  // ratio of decay of chi2 between iteration
  "epsilon" : 0.00100000005
 }

"SimpleTerminationCriteria" { 
  "#id" : 11, 

  // ratio of decay of chi2 between iteration
  "epsilon" : 0.00100000005
 }

"SimpleTerminationCriteria" { 
  "#id" : 13, 

  // ratio of decay of chi2 between iteration
  "epsilon" : 0.00100000005
 }

"MultiTracker3DQR" { 
  "#id" : 14, 
  "name" : "tracker", 

  // computes relative transform between fixed and moving slices
  "aligner" : { 
    "#pointer" : 8
   }, 
  "push_sinks" : [  ], 
  "slice_processors" : [ { 
  "#pointer" : 15
 }, { 
  "#pointer" : 16
 } ], 

  // name of the transform tree to subscribe to
  "tf_topic" : "", 

  // name of the odom topic to propagate to the connected syncs
  "tracker_odom_topic" : "/tracker_odom"
 }

"RobustifierClamp" { 
  "#id" : 17, 

  // threshold of chi after which the kernel is active
  "chi_threshold" : 0.25
 }

"AlignerSliceProcessorProjectiveDepth" { 
  "#id" : 18, 
  "name" : "aligner_slice_projective_depth", 

  // name of the base frame in the tf tree
  "base_frame_id" : "", 
  "diagonal_info_matrix" : [ 1, 1, 10 ], 

  // correspondence finder used in this cue
  "finder" : { 
    "#pointer" : 19
   }, 

  // name of the slice in the fixed scene
  "fixed_slice_name" : "points", 

  // name of the sensor's frame in the tf tree
  "frame_id" : "", 

  // minimum number of correspondences in this slice
  "min_num_correspondences" : 0, 

  // name of the slice in the moving scene
  "moving_slice_name" : "points", 

  // link to a projector where to take the infor for the factor
  "projector" : { 
    "#pointer" : 6
   }, 

  // robustifier used on this slice
  "robustifier" : { 
    "#pointer" : 20
   }
 }

"MergerProjectiveDepthEKF" { 
  "#id" : 21, 

  // toggles point binning (distribution homogenization)
  "enable_binning" : 1, 

  // enables minimal addition of new points (instead all) if merge ratio is not reached
  "enable_conservative_addition" : 0, 

  // landmark estimator used to refine landmark positions in the map (structure-only)
  "landmark_estimator" : { 
    "#pointer" : 22
   }, 

  // maximum permitted correspondence response for merging a point
  "maximum_distance_appearance" : 75, 

  // number of bins in column direction (feature density regulation)
  "number_of_col_bins" : 32, 

  // number of bins in row direction (feature density regulation)
  "number_of_row_bins" : 24, 

  // pinhole projector used for projective merging
  "projector" : { 
    "#pointer" : 6
   }, 

  // target merge ratio (#merges/#correspondences)
  "target_merge_ratio" : 0.5, 

  // target number of points to merge, if hit no further points without correspondences will be added to moving (i.e. determines the pool of trackable points)
  "target_number_of_merges" : 200, 

  // point unprojector to obtain points in camera frame from adapted UV-D measurements
  "unprojector" : { 
    "#pointer" : 23
   }
 }

"RobustifierSaturated" { 
  "#id" : 20, 

  // threshold of chi after which the kernel is active
  "chi_threshold" : 25
 }

"MotionModelConstantVelocity3D" { 
  "#id" : 24, 
  "name" : "motion_model"
 }

"CorrespondenceFinderDescriptorBasedBruteforce2D3D" { 
  "#id" : 25, 
  "name" : "cf_bruteforce_2d", 

  // maximum permitted descriptor distance for a match
  "maximum_descriptor_distance" : 30, 

  // Lowe's distance to drop ambiguous match candidates
  "maximum_distance_ratio_to_second_best" : 0.699999988, 

  // desired minimum matching ratio with current configuration (signals transgressions)
  "minimum_matching_ratio" : 0.25
 }

"IterationAlgorithmLM" { 
  "#id" : 26, 

  // max lm iterations [default: 10]
  "lm_iterations_max" : 100, 

  // upper clamp for lambda if things go well
  "step_high" : 0.666666985, 

  // lower clamp for lambda if things go well 
  "step_low" : 0.333332986, 

  // scale factor for the lambda computed by the system, do not influence the one provided by the user [default: 1e-5]
  "tau" : 9.99999975e-06, 

  // initial lm lambda, if 0 is computed by system [default: 0]
  "user_lambda_init" : 0, 

  // set to true uses lambda*diag(H), otherwise uses lambda*I [default: true]
  "variable_damping" : 1
 }

"CorrespondenceFinderProjectiveCircle3D3D" { 
  "#id" : 19, 
  "name" : "cf_projective_bf", 

  // descriptor distance step size (increase/decrease)
  "descriptor_distance_step_size_pixels" : 5, 

  // maximum permitted descriptor distance for a match
  "maximum_descriptor_distance" : 75, 

  // Lowe's distance to drop ambiguous match candidates
  "maximum_distance_ratio_to_second_best" : 0.699999988, 

  // maximum allowed transform estimate change threshold for assuming convergence
  "maximum_estimate_change_norm_for_convergence" : 9.99999975e-06, 

  // maximum projective region search radius in pixels
  "maximum_search_radius_pixels" : 100, 

  // minimum permitted descriptor distance for a match (initial)
  "minimum_descriptor_distance" : 35, 

  // desired minimum matching ratio with current configuration (signals transgressions)
  "minimum_matching_ratio" : 0.100000001, 

  // minimum number of iterations to perform recomputes (forced)
  "minimum_number_of_iterations" : 5, 

  // minimum projective region search radius in pixels
  "minimum_search_radius_pixels" : 25, 

  // minimum number of solver iterations (on estimate) to await before reprojecting points
  "number_of_solver_iterations_per_projection" : 5, 

  // pinhole projector used for projective descriptor matching
  "projector" : { 
    "#pointer" : 6
   }, 

  // search radius step size (increase/decrease) in pixels
  "search_radius_step_size_pixels" : 5
 }

"InitializerCamera3D" { 
  "#id" : 27, 
  "camera_matrix_owners" : [ { 
  "#pointer" : 6
 }, { 
  "#pointer" : 23
 } ], 

  // topic for the camera info
  "topic" : "/camera/rgb/image_raw/info"
 }

"AlignerSliceProcessor3D" { 
  "#id" : 28, 
  "name" : "aligner_slice_3d", 

  // name of the base frame in the tf tree
  "base_frame_id" : "", 

  // correspondence finder used in this cue
  "finder" : { 
    "#pointer" : -1
   }, 

  // name of the slice in the fixed scene
  "fixed_slice_name" : "points", 

  // name of the sensor's frame in the tf tree
  "frame_id" : "", 

  // minimum number of correspondences in this slice
  "min_num_correspondences" : 0, 

  // name of the slice in the moving scene
  "moving_slice_name" : "points", 

  // robustifier used on this slice
  "robustifier" : { 
    "#pointer" : 17
   }
 }

"MultiAligner3DQR" { 
  "#id" : 8, 
  "name" : "aligner", 

  // toggles additional inlier only runs if sufficient inliers are available
  "enable_inlier_only_runs" : 1, 

  // toggles removal of correspondences which factors are not inliers in the last iteration
  "keep_only_inlier_correspondences" : 1, 

  // maximum number of iterations
  "max_iterations" : 100, 

  // minimum number ofinliers
  "min_num_inliers" : 6, 
  "slice_processors" : [ { 
  "#pointer" : 18
 }, { 
  "#pointer" : 29
 } ], 

  // this solver
  "solver" : { 
    "#pointer" : 30
   }, 

  // termination criteria, not set=max iterations
  "termination_criteria" : { 
    "#pointer" : -1
   }
 }

"LandmarkEstimatorProjectiveDepthEKF3D" { 
  "#id" : 22, 
  "name" : "landmark_estimator", 

  // filter instance used to refine the landmark position estimate
  "filter" : { 
    "#pointer" : 5
   }, 

  // maximum permitted covariance matrix Frobenius norm value for merging
  "maximum_covariance_norm_squared" : 1, 

  // maximum distance in geometry (i.e. 3D point distance L2 norm) in meters squared
  "maximum_distance_geometry_meters_squared" : 0.25, 

  // minimum per element covariance value (prohibits ill-shaped uncertainties)
  "minimum_state_element_covariance" : 0.01
 }

"MultiAligner3DQR" { 
  "#id" : 31, 
  "name" : "loop_aligner", 

  // toggles additional inlier only runs if sufficient inliers are available
  "enable_inlier_only_runs" : 0, 

  // toggles removal of correspondences which factors are not inliers in the last iteration
  "keep_only_inlier_correspondences" : 0, 

  // maximum number of iterations
  "max_iterations" : 10, 

  // minimum number ofinliers
  "min_num_inliers" : 10, 
  "slice_processors" : [ { 
  "#pointer" : 28
 } ], 

  // this solver
  "solver" : { 
    "#pointer" : 9
   }, 

  // termination criteria, not set=max iterations
  "termination_criteria" : { 
    "#pointer" : -1
   }
 }

"LocalMapSplittingCriterionViewpoint3D" { 
  "#id" : 32, 

  // rotation difference between the center of the local maps (in radians)
  "local_map_angle_distance_radians" : 0.25, 

  // distance between the center of the local maps (in meters)
  "local_map_distance" : 1
 }

"RawDataPreprocessorTrackerEstimate3D" { 
  "#id" : 33, 
  "name" : "adaptor_tracker_estimate_buffer", 

  // maximum size of the pose buffer
  "number_of_poses_to_keep" : 5
 }

"IterationAlgorithmGN" { 
  "#id" : 10, 

  // damping factor, the higher the closer to gradient descend. Default:0
  "damping" : 0
 }

"PointIntensityDescriptor3fUnprojectorPinhole" { 
  "#id" : 23, 
  "name" : "unprojector", 

  // end angle    [rad]
  "angle_max" : 3.14159012, 

  // start angle  [rad]
  "angle_min" : -3.14159012, 

  // minimum number of points in ball when computing a valid normal
  "normal_min_points" : 5, 

  // range of points considered while computing normal
  "normal_point_distance" : 0.200000003, 

  // number of laser beams
  "num_ranges" : 0, 

  // max laser range [m]
  "range_max" : 10, 

  // min laser range [m]
  "range_min" : 0.00100000005
 }

"PipelineRunner" { 
  "#id" : 34, 
  "name" : "runner", 
  "push_sinks" : [ { 
  "#pointer" : 35
 } ], 

  // the source of the pipeline
  "source" : { 
    "#pointer" : 4
   }, 

  // name of the transform tree to subscribe to
  "tf_topic" : ""
 }

"IterationAlgorithmGN" { 
  "#id" : 36, 

  // damping factor, the higher the closer to gradient descend. Default:0
  "damping" : 0.100000001
 }

"MessageSynchronizedSink" { 
  "#id" : 37, 
  "name" : "sync", 

  // name of the frame in the message pack
  "output_frame_id" : "pack_frame_id", 

  // name of the topic in the message pack
  "output_topic" : "pack", 
  "push_sinks" : [ { 
  "#pointer" : 38
 } ], 

  // name of the transform tree to subscribe to
  "tf_topic" : "", 

  // interval for the messages to be sinchronized
  "time_interval" : 0.01, 
  "topics" : [ "/camera/rgb/image_raw", "/camera/depth/image_raw", "/camera/rgb/image_raw/info", "/camera/depth/image_raw/info" ]
 }

"Solver" { 
  "#id" : 39, 
  "actions" : [  ], 

  // pointer to the optimization algorithm (GN/LM or others)
  "algorithm" : { 
    "#pointer" : 26
   }, 

  // pointer to linear solver used to compute Hx=b
  "linear_solver" : { 
    "#pointer" : 1
   }, 
  "max_iterations" : [ 10 ], 

  // Minimum mean square error variation to perform global optimization
  "mse_threshold" : 0, 
  "robustifier_policies" : [  ], 

  // term criteria ptr, if 0 solver will do max iterations
  "termination_criteria" : { 
    "#pointer" : 12
   }, 

  // turn it off to make the world a better place
  "verbose" : 0
 }

"TrackerSliceProcessorProjectiveDepth" { 
  "#id" : 15, 
  "name" : "tracker_slice_projective_depth", 

  // measurement adaptor used in the slice
  "adaptor" : { 
    "#pointer" : 40
   }, 

  // name of the base frame in the tf tree
  "base_frame_id" : "", 

  // clipper used in the slice
  "clipper" : { 
    "#pointer" : 41
   }, 

  // merger used for aligment of local maps in the slice
  "closure_merger" : { 
    "#pointer" : 2
   }, 

  // name of the sensor frame in the tf tree
  "frame_id" : "", 

  // name of the slice in the moving scene
  "measurement_slice_name" : "points", 

  // merger used for aligment of a measurement to a local map in the slice
  "merger" : { 
    "#pointer" : 21
   }, 

  // name of the slice in the fixed scene
  "scene_slice_name" : "points"
 }

"TrackerSliceProcessorEstimationBuffer3D" { 
  "#id" : 16, 
  "name" : "tracker_slice_estimate_buffer", 

  // measurement adaptor used in the slice
  "adaptor" : { 
    "#pointer" : 33
   }, 

  // name of the base frame in the tf tree
  "base_frame_id" : "", 

  // name of the sensor frame in the tf tree
  "frame_id" : "", 

  // name of the slice in the moving scene
  "measurement_slice_name" : "trajectory_chunk", 

  // name of the slice in the fixed scene
  "scene_slice_name" : "trajectory_chunk"
 }

"SceneClipperProjective3D" { 
  "#id" : 41, 
  "name" : "clipper_projective_depth", 

  // pinhole projector used to determine whether points lie in the current view
  "projector" : { 
    "#pointer" : 6
   }
 }

"IntensityFeatureExtractorBinned3D" { 
  "#id" : 42, 

  // OpenCV descriptor type (BRIEF-256, BRIEF-512, ORB-256, FREAK-512, ..)
  "descriptor_type" : "ORB-256", 

  // scalar that maps to the first parameter of the chosen detector (if applicable)
  "detector_threshold" : 5, 

  // OpenCV detector type for point tracking (FAST, MSER, GFTT, BRISK-512, ORB-256, ..)
  "detector_type" : "FAST", 

  // enables non maximum suppression (filtering) of detected features (if applicable)
  "enable_non_maximum_suppression" : 1, 

  // number of detectors on the horizontal image axis (cols)
  "number_of_detectors_horizontal" : 3, 

  // number of detectors on the vertical image axis (rows)
  "number_of_detectors_vertical" : 3, 

  // minimum required distance between detected features (if applicable)
  "target_bin_width_pixels" : 10, 

  // target number of keypoints to detect (accumulative over all detectors)
  "target_number_of_keypoints" : 1000
 }

"MergerCorrespondenceProjectiveDepth3D" { 
  "#id" : 43, 
  "name" : "merger_projective_depth", 

  // toggles point binning (distribution homogenization)
  "enable_binning" : 1, 

  // maximum distance in geometry (i.e. 3D point distance norm) in meters (squared)
  "maximum_distance_geometry_squared" : 0.00999999978, 

  // maximum permitted correspondence response for merging a point
  "maximum_response" : 50, 

  // target number of points to merge, if hit no further points without correspondences will be added to moving (i.e. determines the pool of trackable points)
  "target_number_of_merges" : 200, 

  // point unprojector used to obtain points in camera frame from adapted UV-D measurements
  "unprojector" : { 
    "#pointer" : 23
   }
 }

"MultiLoopDetectorHBST3D" { 
  "#id" : 44, 
  "name" : "loop_detector", 

  // module used to figure out which local maps should be checked
  "local_map_selector" : { 
    "#pointer" : -1
   }, 

  // maximum permitted tree depth (maximum = descriptor bit size)
  "maximum_depth" : 16, 

  // maximum permitted descriptor distance for a match
  "maximum_descriptor_distance" : 25, 

  // maximum descriptor distance permitted for internal merging (HBST)
  "maximum_distance_for_merge" : 0, 

  // maximum size of a leaf (i.e. number of descriptors) required before splitting
  "maximum_leaf_size" : 100, 

  // maximum partitioning tolerance (i.e. 0.0 would be a perfect split)
  "maximum_partitioning" : 0.100000001, 

  // minimum required age difference between query and database reference
  "minimum_age_difference_to_candidates" : 1, 

  // aligner used to register loop closures
  "relocalize_aligner" : { 
    "#pointer" : 31
   }, 

  // maximum chi per inlier for success [chi]
  "relocalize_max_chi_inliers" : 0.0500000007, 

  // minimum number of inliers for success [int]
  "relocalize_min_inliers" : 40, 

  // minimum fraction of inliers over total correspondences [num_inliers/num_correspondences]
  "relocalize_min_inliers_ratio" : 0.5
 }

"MessageSortedSink" { 
  "#id" : 35, 
  "name" : "pipeline", 

  // messages older than this lag that will be blasted, no matta what
  "oblivion_interval" : 5, 
  "push_sinks" : [ { 
  "#pointer" : 37
 } ], 

  // name of the transform tree to subscribe to
  "tf_topic" : "", 

  // lag time to sort messages
  "time_interval" : 0.01, 

  // if set prints crap
  "verbose" : 0
 }

"RawDataPreprocessorMonocularDepth" { 
  "#id" : 40, 
  "name" : "adaptor_projective_depth", 

  // scaling factor used to obtain depth in meters from pixel values
  "depth_scaling_factor_to_meters" : 0.00100000005, 

  // feature extractor used to detect keypoints and compute descriptors
  "feature_extractor" : { 
    "#pointer" : 42
   }, 

  // topic depth image
  "topic_depth" : "/camera/depth/image_raw", 

  // rgb image topic
  "topic_rgb" : "/camera/rgb/image_raw"
 }

"AlignerSliceMotionModel3D" { 
  "#id" : 29, 
  "name" : "aligner_slice_motion_model", 

  // name of the base frame in the tf tree
  "base_frame_id" : "", 

  // name of the slice in the fixed scene
  "fixed_slice_name" : "trajectory_chunk", 

  // name of the sensor's frame in the tf tree
  "frame_id" : "", 

  // model used to estimate inter-frame motion
  "motion_model" : { 
    "#pointer" : 24
   }, 

  // name of the slice in the moving scene
  "moving_slice_name" : "trajectory_chunk", 

  // robustifier used on this slice
  "robustifier" : { 
    "#pointer" : -1
   }
 }

"CorrespondenceFinderProjectiveKDTree3D3D" { 
  "#id" : 45, 
  "name" : "cf_projective_kd", 

  // descriptor distance step size (increase/decrease)
  "descriptor_distance_step_size_pixels" : 5, 

  // maximum permitted descriptor distance for a match
  "maximum_descriptor_distance" : 50, 

  // Lowe's distance to drop ambiguous match candidates
  "maximum_distance_ratio_to_second_best" : 0.899999976, 

  // maximum allowed transform estimate change threshold for assuming convergence
  "maximum_estimate_change_norm_for_convergence" : 9.99999975e-06, 

  // maximum projective region search radius in pixels
  "maximum_search_radius_pixels" : 100, 

  // minimum permitted descriptor distance for a match (initial)
  "minimum_descriptor_distance" : 25, 

  // desired minimum matching ratio with current configuration (signals transgressions)
  "minimum_matching_ratio" : 0.5, 

  // minimum number of iterations to perform recomputes (forced)
  "minimum_number_of_iterations" : 10, 

  // minimum number of points in the clusters
  "minimum_number_of_points_per_cluster" : 10, 

  // minimum projective region search radius in pixels
  "minimum_search_radius_pixels" : 100, 

  // minimum number of solver iterations (on estimate) to await before reprojecting points
  "number_of_solver_iterations_per_projection" : 25, 

  // pinhole projector used for projective descriptor matching
  "projector" : { 
    "#pointer" : 6
   }, 

  // search radius step size (increase/decrease) in pixels
  "search_radius_step_size_pixels" : 5
 }

"MultiGraphSLAM3D" { 
  "#id" : 38, 
  "name" : "slam", 

  // validator used to confirm loop closures
  "closure_validator" : { 
    "#pointer" : -1
   }, 

  // global solver for loop closures
  "global_solver" : { 
    "#pointer" : 39
   }, 

  // initialization algorithm
  "initializer" : { 
    "#pointer" : 27
   }, 

  // detector used to produce loop closures
  "loop_detector" : { 
    "#pointer" : 44
   }, 
  "push_sinks" : [  ], 

  // relocalizer to avoid creation of new nodes
  "relocalizer" : { 
    "#pointer" : 7
   }, 

  // heuristics that determine when a new local map has to be generated
  "splitting_criterion" : { 
    "#pointer" : 32
   }, 

  // name of the transform tree to subscribe to
  "tf_topic" : "", 

  // incremental tracker
  "tracker" : { 
    "#pointer" : 14
   }
 }

"Solver" { 
  "#id" : 30, 
  "actions" : [  ], 

  // pointer to the optimization algorithm (GN/LM or others)
  "algorithm" : { 
    "#pointer" : 36
   }, 

  // pointer to linear solver used to compute Hx=b
  "linear_solver" : { 
    "#pointer" : 1
   }, 
  "max_iterations" : [ 1 ], 

  // Minimum mean square error variation to perform global optimization
  "mse_threshold" : -1, 
  "robustifier_policies" : [  ], 

  // term criteria ptr, if 0 solver will do max iterations
  "termination_criteria" : { 
    "#pointer" : 13
   }, 

  // turn it off to make the world a better place
  "verbose" : 0
 }

