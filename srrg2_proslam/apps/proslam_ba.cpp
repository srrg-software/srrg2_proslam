#include <iostream>
#include <thread>
#include <srrg_solver/solver_core/factor_graph.h>
#include <srrg_solver/solver_core/iteration_algorithm_gn.h>
#include <srrg_solver/solver_core/solver_stats.h>
#include <srrg_solver/variables_and_factors/types_3d/se3_pose_pose_geodesic_error_factor.h>
#include <srrg_solver/variables_and_factors/types_3d/variable_se3.h>
#include <srrg_solver/variables_and_factors/types_common/all_types.h>
#include <srrg_solver/variables_and_factors/types_projective/se3_pose_point_rectified_stereo_error_factor.h>
#include <srrg_system_utils/parse_command_line.h>
#include <srrg_config/configurable_manager.h>
#include <srrg2_slam_interfaces/instances.h>
#include <unistd.h>

using namespace srrg2_core;
using namespace srrg2_solver;
using namespace std;
using namespace srrg2_slam_interfaces;

// clang-format off
const char* banner[] = {"This program computes the perspective change and global structure for a sequence of images",
                        "The 3D points are anchored in the leftmost camera coordinate frame",
                        0};

// clang-format on
using BALandmark = Eigen::Vector3f;

struct BAStereoMeasurement {
  EIGEN_MAKE_ALIGNED_OPERATOR_NEW
  BAStereoMeasurement(int fid, int lid, const Eigen::Vector4f& meas, const uint8_t* desc=0):
    frame_id(fid),
    landmark_id(lid),
    stereo_measurement(meas){
    if (desc)
      memcpy(descriptor, desc, 32);
    else
      memset(descriptor, 0, 32);
  }
  int frame_id;
  int landmark_id;
  Vector4f stereo_measurement;
  uint8_t descriptor[32];
};

struct BAFrame {
  EIGEN_MAKE_ALIGNED_OPERATOR_NEW
  Eigen::Isometry3f pose;
  std::list<BAStereoMeasurement, Eigen::aligned_allocator<BAStereoMeasurement> > measurements;
  std::list<int> landmark_ids;
};

using BAFramePtr=std::shared_ptr<BAFrame>;
 


template<typename T>
bool parse(T& v,
           istream& is,
           const std::string& key,
           bool read_line) {
  if (read_line) {
    char buf[1024];
    is.getline(buf, 1024);
    if (! is.good())
      return false;
    istringstream ls(buf);
    return parse(v,ls,key,false);
  } else {
    std::string token;
    is >> token;
    if (token!=key) {
      cerr << "wrong token ["<< key<<"]" << " got [" << token << "]" << endl;
      exit(0);
      return false;
    }
    for (int r=0; r<v.rows(); ++r)
      for (int c=0; c<v.cols(); ++c)
        is >> v(r,c);
    return true;
  }
}
  
template <>
bool parse<int>(int& v, istream& is, const std::string& key, bool read_line) {
  if (read_line) {
    char buf[1024];
    is.getline(buf, 1024);
    if (! is.good())
      return false;
    istringstream ls(buf);
    return parse(v,ls,key,false);
  } else {
    std::string token;
    is >> token;
    if (token!=key) {
      cerr << "wrong token ["<< key<<"]" << " got [" << token << "]" << endl;
      exit(0);
      return false;
    }
    is >> v;
    return true;
  }
}

struct BAProblem{
  BAProblem(FactorGraph& pose_graph_):
    _pose_graph(pose_graph_){}

  FactorGraph output_graph;
  std::list<IdentifiablePtr> serialization_order;
  

  FactorGraph& _pose_graph;
  Isometry3f _extrinsics;
  std::map<int, BALandmark, std::less<int>, Eigen::aligned_allocator<BALandmark> > _landmarks;
  std::map<int, std::shared_ptr<VariablePoint3> > _landmark_factors;
  std::map<int, BAFramePtr> _frames;
  int frame_number=0;

  Matrix3f _camera_matrix;
  Isometry3f _sensor_pose;
  Vector2f _image_sizes;
  Vector3f _baseline_pixels;

  std::shared_ptr<VariableMatrix3_4> projection_matrix_v;
  std::shared_ptr<VariablePoint3> image_sizes_v;
  std::shared_ptr<VariableSE3QuaternionRight> previous_pose;
  
  bool parseParameters(istream& fs) {
    if (!parse(_camera_matrix, fs, "camera_matrix:", true))
      return false;
    if (!parse(_sensor_pose.matrix(), fs, "sensor_pose:", true))
      return false;
    if (!parse(_extrinsics.matrix(), fs, "extrinsics:", true))
      return false;

    if (! parse(_image_sizes, fs, "image_sizes:", true))
      return false;

    if (! parse(_baseline_pixels, fs, "baseline_pixels:", true))
      return false;
    cerr << "parse_ok" << endl;
    Eigen::Matrix<float,3,4> projection_matrix;
    Isometry3f inv_pose=_extrinsics.inverse();
    projection_matrix.block<3,3>(0,0)=_camera_matrix*inv_pose.linear();
    projection_matrix.block<3,1>(0,3)=_camera_matrix*inv_pose.translation();
    projection_matrix_v.reset(new VariableMatrix3_4);
    projection_matrix_v->setEstimate(projection_matrix);
    image_sizes_v.reset(new VariablePoint3);
    image_sizes_v->setEstimate(Vector3f(_image_sizes.x(), _image_sizes.y(), _baseline_pixels.x()));
    return true;
  }
  
  bool parseFrame(istream& fs) {
    Eigen::Isometry3f local_map_iso=Eigen::Isometry3f::Identity();

    char buf[1024];
    fs.getline(buf, 1024);
    if (! fs.good()) {
      cerr << "bad stream" << endl;
      return false;
    }
    std::string token;
    istringstream ls(buf);
    ls >> token;
    if (token!="FRAME") {
      return false;
    }
    int lmap_id;
    if (! parse(lmap_id, fs, "lmap_id:", true))
      return false;
    Vector6f p_ext;
    if (! parse(p_ext, fs, "ext:", true))
      return false;
    _extrinsics=geometry3d::v2t(p_ext);
    Vector6f pose_in_local_map;
    if (! parse(pose_in_local_map, fs, "r2l:", true))
      return false;
    VariableBase* pose_var=_pose_graph.variable(lmap_id);
    if (! pose_var) {
      cerr << "no var with id [" << lmap_id << "] in graph" << endl;
    }
    LocalMap3D* pv=dynamic_cast<LocalMap3D*>(pose_var);
    if (! pv) {
      cerr << "unable to retrieve pose of local map in graph" << endl;
      exit(-1);
    }
    local_map_iso=pv->estimate();
    BAFramePtr frame(new BAFrame);
    frame->pose=local_map_iso*geometry3d::v2t(pose_in_local_map);
    Isometry3f camera_pose=frame->pose*_extrinsics;
    int num_landmarks;
    if (! parse(num_landmarks, fs, "landmarks:", true))
      return false;
    for (int i=0; i<num_landmarks; ++i) {
      fs.getline(buf, 1024);
      if (! fs.good())
        return false;
      istringstream ls(buf);
      int landmark_id;
      if (! parse(landmark_id, ls, "id:",false))
        return false;
      Vector3f landmark_pose;
      if (! parse(landmark_pose, ls, "pos:", false))
        return false;
      _landmarks[landmark_id]=camera_pose*landmark_pose;
      frame->landmark_ids.push_back(landmark_id);
    }
    int num_measurements;
    if (! parse(num_measurements, fs, "measurements:", true))
      return false;
    for (int i=0; i<num_measurements; ++i) {
      fs.getline(buf, 1024);
      if (! fs.good())
        return false;
      istringstream ls(buf);
      int lid;
      if (! parse(lid, ls, "lid:",false))
        return false;
      int mid;
      if (! parse(mid, ls, "mid:",false))
        return false;
      Eigen::Vector4f pixel_poses;
      if (! parse(pixel_poses, ls, "imc:",false))
        return false;
      Eigen::Matrix<int, 3, 1> des_sizes;
      if (! parse(des_sizes, ls, "des:",false))
        return false;
      std::string des;
      ls >> des;
      if (_landmarks.find(lid)==_landmarks.end()) {
        cerr << "measurement to non existing landmark" << endl;
        exit(0);
      }
      frame->measurements.push_back(BAStereoMeasurement(frame_number, lid, pixel_poses));
    }
    _frames[frame_number]=frame;
    ++frame_number;
    return true;
  }

  void makeGraph(FactorGraph& out_graph) {
    previous_pose.reset();
    serialization_order.clear();
    projection_matrix_v->setStatus(VariableBase::Fixed);
    projection_matrix_v->setGraphId(_frames.size());
    image_sizes_v->setStatus(VariableBase::Fixed);
    image_sizes_v->setGraphId(projection_matrix_v->graphId()+1);
    out_graph.addVariable(projection_matrix_v);
    out_graph.addVariable(image_sizes_v);
    int landmark_offset_id = image_sizes_v->graphId()+1;
    serialization_order.push_back(projection_matrix_v);
    serialization_order.push_back(image_sizes_v);
    for (auto& f_it: _frames) {
      VariableSE3QuaternionRight* pose_v=new VariableSE3QuaternionRight;
      int f_id = f_it.first;
      BAFrame&  f=*f_it.second;
      pose_v->setGraphId(f_id);
      pose_v->setEstimate(f.pose);
      
      std::shared_ptr<VariableSE3QuaternionRight> v_pose_ptr(pose_v);
      out_graph.addVariable(v_pose_ptr);
      serialization_order.push_back(v_pose_ptr);

      if (previous_pose) {
        std::shared_ptr<SE3PosePoseGeodesicErrorFactor> odom(new SE3PosePoseGeodesicErrorFactor);
        odom->setVariableId(0, previous_pose->graphId());
        odom->setVariableId(1, v_pose_ptr->graphId());
        odom->setMeasurement(previous_pose->estimate().inverse()*v_pose_ptr->estimate());
        Matrix6f info;
        info.setIdentity();
        info*=1e-3;
        odom->setInformationMatrix(info);
        out_graph.addFactor(odom);
        serialization_order.push_back(odom);
      } else {
        pose_v->setStatus(VariableBase::Fixed);
      }
      previous_pose=v_pose_ptr;

      
      for (int landmark_id: f.landmark_ids) {
        const auto& l=_landmarks[landmark_id];
        auto v_point = new VariablePoint3;
        auto v_point_ptr = std::shared_ptr<VariablePoint3>(v_point);
        v_point->setGraphId(landmark_offset_id+landmark_id);
        v_point->setEstimate(l);
        out_graph.addVariable(v_point_ptr);
        serialization_order.push_back(v_point_ptr);
        _landmark_factors[landmark_id]=v_point_ptr;
      }

      for (auto& m: f.measurements) {
        SE3PosePointRectifiedStereoErrorFactor* factor=new SE3PosePointRectifiedStereoErrorFactor;
        factor->setMeasurement(Eigen::Vector3f(m.stereo_measurement(0),
                                               m.stereo_measurement(1),
                                               m.stereo_measurement(0)-m.stereo_measurement(3)));
        factor->setVariableId(0,pose_v->graphId());
        auto& v_landmark=_landmark_factors[m.landmark_id];
        factor->setVariableId(1,v_landmark->graphId()); 
        factor->setVariableId(2,projection_matrix_v->graphId());       
        factor->setVariableId(3,image_sizes_v->graphId());
        Vector3f landmark_pose;
        if (factor->triangulate(landmark_pose,
                                factor->measurement(),
                                *pose_v,
                                *projection_matrix_v,
                                *image_sizes_v)) {
          v_landmark->setEstimate(landmark_pose);
        }
        FactorBasePtr factor_ptr(factor);
        out_graph.addFactor(factor_ptr);
        serialization_order.push_back(factor_ptr);
      }
    }
  }
};

int main(int argc_, char** argv_) {
  srrg2_slam_interfaces::srrg2_slam_interfaces_registerTypes();
  
  // ds set up CLI parameters
  // clang-format off
  ParseCommandLine command_line_parser(argv_, banner);
  ArgumentString dump_file (&command_line_parser, "i",
                            "dump", "dump file from proslam", "");
  ArgumentString graph_file(
    &command_line_parser, "g", "graph",
                          "pose_graph drom proslam", "");

  ArgumentString output_file(&command_line_parser, "oi", "output-initial",
                            "starting graph to be loaded with solver", "");

  ArgumentString config_file(&command_line_parser, "c", "config",
                             "config file with the solver", "");

  ArgumentString output_opt_file(&command_line_parser, "oo", "output-optimized",
                            "graph after optimization (pruned of outliers)", "");
  
  command_line_parser.parse();
  
  if (! dump_file.isSet()) {
    cerr << "provide a dump file" << endl;
  }
  if (! graph_file.isSet()) {
    cerr << "provide a graph file" << endl;
  }

  auto pose_graph=FactorGraph::read(graph_file.value());
  
  cerr << "#variables " << pose_graph->variables().size() << endl;
  cerr << "#factors " << pose_graph->factors().size() << endl;
  BAProblem problem(*pose_graph);
  ifstream is(dump_file.value());
  cerr << "opeing file [" << dump_file.value() << "]" << endl;
  cerr << "parameters: " << problem.parseParameters(is) << endl;;
  while (is.good()) {
    problem.parseFrame(is);
  }

  cerr << "preassembled BA problem with" << endl;
  cerr << "# poses: " << problem._frames.size() << endl;
  cerr << "# unique landmarks: " << problem._landmarks.size() << endl;
  size_t num_meas=0;
  for (auto& f: problem._frames) {
    num_meas += f.second->measurements.size();
  }
  cerr << "# measurements: " << num_meas << endl;

  cerr << "assembling graph" << endl;
  FactorGraph out_graph;
  problem.makeGraph(out_graph);
  cerr << "graph has: " << endl;
  cerr << "# variables: " << out_graph.variables().size() << endl;
  cerr << "# factors: " << out_graph.factors().size() << endl;

  if (output_file.isSet()) {
    cerr << "writing output (pre-opt) on file" << endl;
    Serializer ser;
    ser.setFilePath(output_file.value());
    int count=0;
    cerr << endl;
    for (auto& i: problem.serialization_order) {
      ser.writeObject(*i);
      cerr << "\rwriting object " << ++count;
    }
    cerr << endl;
  }

  if (! config_file.isSet()) {
    cerr << "config_file not set, terminating" << endl;
    return 0;
  }
  ConfigurableManager manager;
  manager.read(config_file.value());
  auto solver = manager.getByName<Solver>("solver");
  if (! solver) {
    cerr << "no solver object found in file, aborting" << endl;
    return 0;
  }
  auto algorithm = std::dynamic_pointer_cast<IterationAlgorithmGN>(solver->param_algorithm.value());
  if (! algorithm) {
    cerr << "no GN algorithm in solver, aborting" << endl;
    return 0;
  }

  solver->setGraph(out_graph);

  
  // run with progressively increasing damping
  std::vector<int> dampings({1, 10, 20});
  for (auto d: dampings){
    cerr << "Current Damping: " << d << endl;
    algorithm->param_damping.setValue(d);
    solver->compute();
  }

  // populate a map with the stats for each factor
  size_t k=0;
  const auto& stats=solver->measurementStats();
  std::map<srrg2_solver::FactorBase*, FactorStats> f2s;
  for (auto f_it: out_graph.factors()) {
    f2s[f_it.second]=stats[k++];
  }

  std::set<VariableBase*> bad_vars;
  float min_inlier_ratio=0.4;
  size_t min_times_seen=5;
  
  for (auto v_it: out_graph.variables()) {
    VariableBase* v  = v_it.second;
    VariablePoint3* v_point=dynamic_cast<VariablePoint3*> (v_it.second);
    if (! v_point) {
        continue;
    }
    if (v_point->status()==VariableBase::Fixed)
      continue;
    auto v_factors=out_graph.FactorGraphInterface::factors(v_point);
    int num_inliers=0;
    for (auto f_it: v_factors) {
      if (f2s[f_it.second].status==FactorStats::Inlier)
        ++num_inliers;
    }
    float inlier_ratio=(float)num_inliers/(float)v_factors.size();
    if (v_factors.size() < min_times_seen
        || inlier_ratio < min_inlier_ratio)
        bad_vars.insert(v);
  }
  cerr << "bad vars: " << bad_vars.size() << endl;
  cerr << "good vars: " << out_graph.variables().size() - bad_vars.size() << endl;
  cerr << "suppressing bad variables" << endl;
  for (auto v: bad_vars) {
    auto v_factors=out_graph.FactorGraphInterface::factors(v);
    for (auto f_it: v_factors)
      out_graph.removeFactor(f_it.second);
    out_graph.removeVariable(v);
  }

  if (output_opt_file.isSet()) {
    cerr << "dumping pruned output on file [" << output_opt_file.value() << "]" << endl;
    out_graph.write(output_opt_file.value());
  }
  /*
  ofstream osl("landmarks.dat");
  for(auto& l: problem._landmarks) {
    osl << l.second.transpose() << endl;
  }
  ofstream ost("trj.dat");
  for (auto& t: problem._poses) {
    ost << geometry3d::t2v(t.second).transpose() << endl;
  }
  */
  // int num_parameters=2;
  // int offset_landmark_id=problem._frames.size();
  
}
